//
//  ViewController.swift
//  GoogleAPIs
//
//  Created by Aina Jain on 22/03/17.
//  Copyright © 2017 mphasis.com. All rights reserved.
//

import UIKit
import GoogleMaps

class ViewController: UIViewController, KMLParsingDelegate {

    private var kmlParser: KMLParsing!
    var mapView : GMSMapView?
    let coordinate : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude:  12.00, longitude: 77.00)

    override func viewDidLoad() {
        super.viewDidLoad()
        let url = Bundle.main.url(forResource: "IND_adm0", withExtension: "kml")!
        kmlParser = KMLParsing(url: url)
        kmlParser.delegate  = self
        let data = kmlParser.parseKML()
        print(data ?? "couldn't parse")
        
        let camera = GMSCameraPosition.camera(withTarget: coordinate, zoom: 6.0)
        mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        view = mapView
    }

    func parsingCompletedWithPath(path :GMSMutablePath) {
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 10.0
        polyline.geodesic = true
        polyline.strokeColor = .blue
        polyline.map = mapView
    }

    func parsingCompletedWithError(error:Error) {
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

