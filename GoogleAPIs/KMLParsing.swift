//
//  KMLParsing.swift
//  GoogleAPIs
//
//  Created by Aina Jain on 05/04/17.
//  Copyright © 2017 mphasis.com. All rights reserved.
//

import Foundation
import CoreLocation
import GoogleMaps
struct ELTYPE {
    var typeName: String
    init(_ typeName: String) {self.typeName = typeName}
    
}

enum ElementType:String {
    case coordinates, LinearRing
}

@objc protocol KMLParsingDelegate : NSObjectProtocol {
    func parsingCompletedWithPath(path :GMSMutablePath)
    func parsingCompletedWithError(error:Error)
}
class KMLParsing: NSObject, XMLParserDelegate
{
    let path  = GMSMutablePath()
    var delegate :KMLParsingDelegate?
    var strXMLData:String = ""
    var currentElement = ""
    var arrayCoordinates = [[CLLocationCoordinate2D]]()
    
    var passData:Bool=false
    var passName:Bool=false
    
    
    private var _xmlParser: XMLParser!
    
    init(url: URL) {
        super.init()

        _xmlParser = XMLParser(contentsOf: url)
        _xmlParser.delegate = self
    }
    
    func parseKML() -> String? {
        
        let success:Bool = _xmlParser.parse()
        
        if success
        {
            return strXMLData
        }
        else
        {
            return nil
        }
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        
        switch elementName
        {
        case ElementType.coordinates.rawValue:
            currentElement = elementName
        case ElementType.LinearRing.rawValue:
            currentElement = elementName
        default:
            break
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        switch elementName
        {
        case ElementType.coordinates.rawValue:
            currentElement = ""
        case ElementType.LinearRing.rawValue:
            currentElement = ""
        default:
            break
        }

    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        

        
        switch currentElement {
        case ElementType.coordinates.rawValue :
            let points = string.components(separatedBy: ",")
            if points.count == 2
            {
            path.add(CLLocationCoordinate2D(latitude: Double((points.first! as NSString).doubleValue), longitude: Double((points.last! as NSString).doubleValue)))
            }
            else {
                print("string ::\(string)")
            }
        case ElementType.LinearRing.rawValue :
            strXMLData = ""
            print("LinearRing :::\(string)")
        default:
            currentElement = ""
        }

        
    }
    
    func parserDidEndDocument(_ parser: XMLParser) {
        print("path.count ::\(path.count())")
        if let delegate = delegate , delegate .responds(to: #selector(KMLParsingDelegate.parsingCompletedWithPath(path:))) {
            delegate .parsingCompletedWithPath(path: path)
        }
    }
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        
        if let delegate = delegate , delegate .responds(to: #selector(KMLParsingDelegate.parsingCompletedWithError(error:))) {
            delegate .parsingCompletedWithError(error: parseError)
        }
    }
    
}
